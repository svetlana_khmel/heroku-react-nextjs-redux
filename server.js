const express = require('express');
const next = require('next');
const LRUCache = require('lru-cache');

const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 8080;

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const config = require('./config');

const { join } = require('path');

const Message = require('./model/message');
const User = require('./model/user');

const isOnline = require('is-online');

const { parse } = require('url');
const mongoose = require('mongoose');
const compression = require('compression');

//const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://test2:test2@ds227045.mlab.com:27045/lyrics';

//const MONGODB_URI = 'mongodb://localhost:27017/test';
const MONGODB_URI = 'mongodb://localhost:27017/test';

//const MONGODB_URI = 'mongodb://test2:test2@ds059546.mlab.com:59546/lyricser';

app.prepare()
    .then(() => {
        const server = express();
        //server.use(compression());
        server.use(bodyParser.urlencoded({extended: false}));
          // Parse application/json
          server.use(bodyParser.json());

          // Allows for cross origin domain request:
          server.use(function (req, res, next) {
              res.header('Access-Control-Allow-Origin', '*');
              res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
              next()
          });

          // Request body parsing middleware should be above methodOverride

          server.use(cookieParser());

        // MongoDB
      mongoose.connect(MONGODB_URI, {useMongoClient: true});
      //mongoose.connect(config.database, {useMongoClient: true});


      // Verify username and password, if passed, we return jwt token for client
        // We also include xsrfToken for client, which will be used to prevent CSRF attack
        // and, you should use random complicated key (JWT Secret) to make brute forcing token very hard

        server.post('/api/authenticate', (req, res) => {
            const { username, password } = req.body;

            User.findOne({username: req.body.username}, function (err, user) {
                if (err) throw err;

                if (!user) {

                    res.json({ success: false, message: 'Authentication failed. User not found.' });

                } else if (user) {
                    //check if password matches
                    if (user.password != req.body.password) {

                        res.status(400).json({
                            success: false,
                            message: 'Authentication failed'
                        })

                    } else {
                        // if user is found and password is right

                           let token = createToken(username);

                            res.status(200).json({
                                success: true,
                                message: 'Enjoy your token',
                                token: token,
                                user
                            });
                    }
                }
            });
        });

        // Authenticate middleware
        // We will apply this middleware to every route except '/login' and '/_next'

        server.use(unless(['/login', '/_next'], (req, res, next) => {
            const token = req.cookies['x-access-token'] || req.body.token || req.headers.token;

            if (token) {
                jwt.verify(token, 'jwtSecret', (err, decoded) => {
                    if (err) {
                        res.redirect('/login');
                    } else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;
                        next();
                    }
                })
            } else if (req.url === '/api/register') {
              console.log("reg:::::::::::::::::::::::");
                let newUser = new User(req.body.data);

                User.findOne({username: req.body.data.username}, function (err, user) {
                    if (user) {
                        res.send({error: "User has already registred. Please, choose another username.", status: 'found'});
                    } else {
                      console.log("ELSE:::::::::::::::::::::::");
                        newUser.save(function (err, resp) {
                          console.log("ELSE:::::::::::::::::::::::", err);
                          console.log("ELSE:::::::::::::::::::::::", resp);

                            let token =  createToken(newUser.username);

                            // return the information including token as JSON
                            res.json({
                                success: true,
                                message: 'Enjoy your token!',
                                token: token,
                                user: newUser,
                                data: resp
                            });
                        });
                    }
                });

            } else {
                res.redirect('/login');
            }
        }));

        // Api example to prevent CRSF attack
        server.post('/api/preventCRSF', (req, res, next) => {
            if (req.decoded.xsrfToken === req.get('X-XSRF-TOKEN')) {
                res.status(200).json({
                    success: true,
                    message: 'Yes, this api is protected by CRSF attack'
                })
            } else {
                res.status(400).json({
                    success: false,
                    message: 'CRSF attack is useless'
                })
            }
        });

        server.get('/', (req, res) => {
            return handle(req, res)
        });

        server.get('*', (req, res) => {
            const parsedUrl = parse(req.url, true);
            const { pathname, query } = parsedUrl;

            if (pathname === '/api/article') {

                const userdata =  JSON.parse(req.headers.userdata);

                Message.find({user_id: userdata.id}, function (err, result) {

                    if (err) return next(err);

                    res.json(result);
                });

            } else if (pathname === '/b') {
                app.render(req, res, '/a', query);

            } else if (pathname === '/api/online-status') {
                isOnline().then(online => {
                    console.log('online status: ', online);
                    //let resp = JSON.stringify({status:online});
                    //res.json(resp);
                    res.send(online);
                    //=> true
                });
            } else if (pathname === '/service-worker.js') {
                    const filePath = join(__dirname, '.next', pathname);
                    app.serveStatic(req, res, filePath)
            } else {
                handle(req, res, parsedUrl)
            }
        });

        server.get('/contact', (req, res) => {
            return handle(req, res)
        });

        server.get('/new', (req, res) => {
            return handle(req, res)
        });

        server.get('/edit', (req, res) => {
            return handle(req, res)
        });

        server.get('/about', (req, res) => {
            return handle(req, res)
        });

        server.post('/api/article', (req, res) => {
            //req.body.user = req.user;

            const userdata =  JSON.parse(req.headers.userdata);

            let data = req.body.data;
            data.user = req.user;
            user_id = userdata.id;

            let message = new Message({
                data,
                user_id
            });

            message.save(function (err, product, numAffected) {
                res.json(message);
            });
        });

        server.put('/api/article/:id', function (req, res) {

            Message.findByIdAndUpdate(req.params.id, req.body, function(err, post) {

                if (err) return next(err);
                // res.json(post);

                getMessage(req.params.id, req, res);
            });
        });

        server.delete('/api/article/:id', (req, res) => {

            Message.findByIdAndRemove(req.params.id, function (err, post) {
                if (err) return next(err);
                res.json(post);
            });
        });

        server.listen(port, (err) => {
            if (err) throw err;
            console.log('> Ready on http://localhost:' + port);
        })
    })
    .catch((ex) => {
        console.error(ex.stack);
        process.exit(1)
    });

function unless (paths, middleware) {
    return function(req, res, next) {
        let isHave = false;
        paths.forEach((path) => {
            if (path === req.path || req.path.includes(path)) {
                isHave = true;
                return;
            }
        });
        if (isHave) {
            return next()
        } else {
            return middleware(req, res, next)
        }
    }
}

function getMessage (id, req, res) {
    Message.findById(id, function(err, post){

        if (err) return next(err);
        res.json(post);
    });
}

function createToken(username) {

    var token = jwt.sign({
        username: username,
        xsrfToken: crypto.createHash('md5').update(username).digest('hex')
    }, 'jwtSecret', {
        expiresIn: 60*60
    });

    return token;
}
