module.exports = {
  'secret': 'ilovescotchyscotch',
  'database': 'mongodb://test2:test2@ds059546.mlab.com:59546/lyricser',
  'productionPathHeroku': 'https://lyricser.herokuapp.com',
  'productionPath': 'https://ec2-18-219-115-83.us-east-2.compute.amazonaws.com/:8080',
  'localPath': 'http://localhost:8080'
}
