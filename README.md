# README #

### What is this repository for? ###

* Next.js server rendering,
* form handling with Next.js
* React,
* Redux,
* Mongo, Mongoose,
* JWT authentication,
* Material UI
* isomorphic-fetch
* manifest.xml
* mobile, responsive

Screenshot:

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1515501699/Screen_Shot_2018-01-09_at_2.37.41_PM_vzcvod.png)


## Application is available on http://lyricser.herokuapp.com



https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27

### How do I get set up? ###

* Summary of set up

npm install

npm run build

npm start

* How to run tests


* Deployment instructions

### Handfull articles ###

* Debugger in node.js

* https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27

* https://habrahabr.ru/post/323588/
* https://medium.com/styled-components/building-a-blog-with-next-js-359cf1236574

* https://staart.nmr.io/

https://medium.com/@positivecarlos/authentication-on-universal-react-with-next-js-b441ef458046
https://medium.com/@positivecarlos/authentication-on-universal-react-with-next-js-b441ef458046

# Good example: #

* https://github.com/timberio/next-go

# MongoBD and next.js #

* http://thecodebarbarian.com/building-a-nextjs-app-with-mongodb.html?utm_source=hashnode.com

* https://github.com/vkarpov15/thecodebarbarian.com/blob/master/lib/posts/20170426_nextjs_mongodb.md

* https://github.com/tomsoderlund/nextjs-express-mongoose-crudify-boilerplate


# JWT Authorisation example #

* https://github.com/zeit/next.js/issues/153

* https://github.com/trandainhan/next.js-example-authentication-with-jwt


# Building a Blog With Next.js and styled-components #

* https://medium.com/styled-components/building-a-blog-with-next-js-359cf1236574

* https://github.com/timberio/next-go/tree/master/components

* https://github.com/fridays/next-routes

# Use ESLint Like a Pro with ES6 and React #

* http://www.zsoltnagy.eu/use-eslint-like-a-pro-with-es6-and-react/

# Conditional rendering:#

* http://devnacho.com/2016/02/15/different-ways-to-add-if-else-statements-in-JSX/



 # Pure javascript immutable arrays

 * https://vincent.billey.me/pure-javascript-immutable-array/
 * https://redux.js.org/docs/recipes/reducers/ImmutableUpdatePatterns.html

# Styles #

* https://reactstrap.github.io/components/navbar/
* https://www.npmjs.com/package/react-material-design
* http://docs.nativebase.io/docs/examples/navigation/StackNavigationExample.html

# material-ui #
* https://www.npmjs.com/package/material-ui
* https://material.io/icons/
https://www.npmjs.com/package/react-material-design


### How to debug ###

* Add debugger to getInitialProps
* Setup a custom server on the backend so that you can call node server.js instead of next start (explained here)
* Run node --inspect server.js (I think you need node version >= 8.0.0)
* If in chrome, open the node debugger: go to about:inspect and click "Open dedicated DevTools for Node"
* Load the page with debugger in getInitialProps - it should pause execution for you to debug

* heroku logs --source app

### App is available on AWS ###
http://ec2-18-219-115-83.us-east-2.compute.amazonaws.com:3200/

1. Go to .ssh to get aws c2 console
2. Run
ssh -i "lyricser.pem" ubuntu@ec2-18-219-115-83.us-east-2.compute.amazonaws.com


* Run node as a daemon:

* https://www.npmjs.com/package/pm2 (Run server forever: pm2 start server.js -i max)

* https://www.terlici.com/2015/06/20/running-node-forever.html

* How to install mongodb with aws ec2:

* https://optimalbi.com/blog/2017/09/14/how-to-install-mongodb-with-aws-ec2/

* How To Install Git on Ubuntu 14.04

* https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-14-04

* Restoring single collection in an existing mongodb

* mongorestore --db mydbname --collection mycollection dump/mydbname/mycollection.bson

* Command history is stored in .bash_history




