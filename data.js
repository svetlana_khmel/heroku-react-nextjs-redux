const loginUrl = '/auth/login';
const registerUrl = '/auth/register';
const getMessageUrl = '/api/messages';
const editArticleUrl = '/auth/editArticle';
const deleteArticleUrl = '/auth/deleteArticle';
const addArticleUrl = '/api/message';

const data = {
    getMessages (token) {
        let lyricsData;

        if (navigator.onLine === false ) {
            if(typeof Storage !== 'undefined' && localStorage.getItem('lyricsData').length !== 0) {
                let promise = new Promise((resolve, reject) =>{
                    resolve(lyricsData)
                })
            }
        }

        return fetch(getMessageUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'Content-Type': 'text/html',
            },
            body: JSON.stringify({
                token
            })
        });
    },

    loginUser (data) {
        return fetch(loginUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'Content-Type': 'text/html'
            },
            body: JSON.stringify(
                data
            )
        });
    },

    registerUser (data) {
        return fetch(registerUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data
            })
        })
    },

    addArticle(token, data) {
        return fetch(addArticleUrl, {
            method: 'post',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data,
                token
            })
        });
    },

    editArticle(token, id, data) {
        return fetch(`${editArticleUrl}/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data,
                token
            })
        });
    },

    deleteArticle(id) {
        return fetch(`${deleteArticleUrl}/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            }
        });
    }
};

export default data;
