import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'

import Register from '../components/Register'
import Login from '../components/Login'
import stylesheet from 'styles/login.scss'
import Head from 'next/head'

import { initStore } from '../store'

class LoginPage extends Component {
    render () {
        return <div>
            <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
            <Head>
                <title>Sing-Song</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet" />
                <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css' />

                <link rel="manifest" href="/public/manifest.json" />
                <link rel="apple-touch-icon" sizes="180x180" href="/public/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png" />
                <link rel="mask-icon" href="/public/safari-pinned-tab.svg" color="#5bbad5" />
                <meta name="theme-color" content="#ffffff" />
            </Head>
            <Register />
            <Login />
        </div>
    }
}

export default withRedux(initStore, null)(LoginPage);