import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import Main from '../components/index'

import { initStore } from '../store'


import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {green100, green500, green700} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: green500,
        primary2Color: green700,
        primary3Color: green100,
    },
}, {
    avatar: {
        borderColor: null,
    }
});

class Index extends Component {
    componentDidMount () {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/service-worker.js')
                .then(registration => {
                    console.log('service worker registration successful')
                })
                .catch(err => {
                    console.warn('service worker registration failed', err.message)
                })
        }
    }
    render () {
        return <MuiThemeProvider muiTheme={muiTheme}>
            <Main />
        </MuiThemeProvider>
    }
}

export default withRedux(initStore, null)(Index)
