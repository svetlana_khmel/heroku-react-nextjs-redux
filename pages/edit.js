import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import Edit from '../components/Edit'

import { initStore } from '../store'

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {green100, green500, green700} from 'material-ui/styles/colors';
import HeaderDrawer from '../components/headers/HeaderDrawer';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: green500,
    primary2Color: green700,
    primary3Color: green100,
  },
}, {
  avatar: {
    borderColor: null,
  }
});

class Index extends Component {
  render () {
    return <MuiThemeProvider muiTheme={muiTheme}>
      <HeaderDrawer />
      <Edit />
    </MuiThemeProvider>
  }
}

export default withRedux(initStore, null)(Index)