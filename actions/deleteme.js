
// The constructor takes in an array of items and a integer indicating how many
// items fit within a single page
function PaginationHelper(collection, itemsPerPage){
    this.collection = collection;
    this.size = collection.length;
    this.itemsPerPage = itemsPerPage;
}
// returns the number of items within the entire collection
PaginationHelper.prototype.itemCount = function() {
    return size;
}

// returns the number of pages
PaginationHelper.prototype.pageCount = function() {
    var fullPagesDouble = this.collection.length/this.itemsPerPage;
    var fullPagesInt = Math.floor(fullPagesDouble);

    if(fullPagesInt < fullPagesDouble ){
        fullPagesInt += 1;
    }

    return fullPagesInt;
}



// returns the number of items on the current page. page_index is zero based.
// this method should return -1 for pageIndex values that are out of range
PaginationHelper.prototype.pageItemCount = function(pageIndex) {
    var firstOnPage = pageIndex*this.itemsPerPage;
    var lastOnPage = firstOnPage + this.itemsPerPage;
    if(firstOnPage>this.collection.length){
        return -1;
    }
    if(this.collection.length> lastOnPage){
        return this.itemsPerPage;
    }
    return this.collection.length - pageIndex*this.itemsPerPage;

}

// determines what page an item is on. Zero based indexes
// this method should return -1 for itemIndex values that are out of range
PaginationHelper.prototype.pageIndex = function(itemIndex) {

}