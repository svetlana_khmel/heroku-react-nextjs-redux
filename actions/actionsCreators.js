import * as types from '../actionTypes';
import fetch from 'isomorphic-fetch';
import config from '../config';

const script = `${process.env.ENV || 'dev'}`
let path = config.localPath;
//script === 'dev' ? path = config.localPath : path = config.productionPath;
///let path = config.productionPath;
// rename file

//let path = '';

const getMessageUrl = `${path}/api/article`;
const addMessageUrl = `${path}/api/article`;
const deleteArticleUrl = `${path}/api/article`; // deleteArticle -> /article
const editArticleUrl = `${path}/api/article`;
const registerUrl = `${path}/api/register`;
const loginUrl = `${path}/api/authenticate`;
const getOnlineStatusUrl = `${path}/api/online-status`; // + /api + GET

export const inputChange = (title, name, val) => dispatch => {
  return dispatch({ type: types.INPUT_VALUE, title, name, val }) // redundant return, need linter
}

export const getDataCreator = (data) => {
  return {
    type: types.GET_ARTICLES,
    payload: data
  }
}

export const getStoredDataCreator = (data) => {
  return {
    type: types.GET_STOERD_ARTICLES,
    payload: data
  }
}

export const setOnlineStatusCreator = (isOnline) => {
  console.log('..setOnlineStatusCreator...', isOnline)
  return {
    type: types.SET_ONLINE_STATUS,
    isOnline
  }
}

export const doSearchCreator = (data) => {
  return {
    type: types.DO_SEADRCH,
    payload: data
  }
}

export const closeArticleCreator = (isClose) => { // close -> toggle, bull -> shouldClose || isClosed
  return {
    type: types.CLOSE_ARTICLE,
    isClose
  }
}

export const addArticleCreator = (payload) => {
  return {
    type: types.ADD_ARTICLE,
    payload
  }
}

export const addFormValue = (data) => {
  debugger
  return {
    type: 'ADD_FORM_VALUE',
    payload: data
  }
}

export const removeArticle = (id) => {
  return {
    type: types.REMOVE_ARTICLE,
    id
  }
}

export const openArticleForEditCreator = (id, article) => {
  return {
    type: types.OPEN_ARTICLE_TO_EDIT,
    id,
    article
  }
}

export const translateArticle = (id, isTranslated) => {
  return {
    type: types.TRANSLATE_ARTICLE,
    id,
    isTranslated
  }
}

export const translateArticleOffline = (id, isTranslatedOffline, data) => {
  return {
    type: types.TRANSLATE_ARTICLE_OFFLINE,
    id,
    isTranslatedOffline,
    data
  }
}

export const editArticleCreator = (data) => {
  return {
    type: types.EDIT_ARTICLE,
    data
  }
}

export const setUser = (data) => {
  return {
    type: types.LOGIN_USER,
    payload: data
  }
}

export const logOutCreator = () => {
  return {
    type: types.LOGOUT
  }
}

export const createUser = (data) => {
  return {
    type: types.REGISTER_USER,
    payload: data
  }
}

export const setChips = (data) => {
  return {
    type: types.SET_CHIPS,
    payload: data
  }
}

export const loadData = (userdata, token) => { //  use camel-case
  return (dispatch, getState) => {
    return fetch(getMessageUrl, { // consider creating apiClient abstraction of service (like userService) getUser updateUser
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'token': token,
        'userdata': JSON.stringify(userdata)
      }
    })
            .then((response) => response.json())
            .then(data => {
              console.log('REQUEST HAS BEEN MADE and got data   ', data)
              dispatch(getDataCreator(data))
            })
            .catch((err) => console.log('Load error', err))
  }
}

export const setOnlineStatus = (userdata, token) => {
  return (dispatch) => {
    return fetch(getOnlineStatusUrl, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'token': token,
        'userdata': JSON.stringify(userdata)
      }
    })
            .then((response) => response.json())
            .then(data => {
              dispatch(setOnlineStatusCreator(data))
            })
            .catch((err) => console.log('Load error', err))
  }
}

export const loginUser = (userdata) => {
  return (dispatch) => {
    return fetch(loginUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userdata)
    })
        .then((response) => response.json())
        .then((data) => dispatch(setUser(data)))
        .catch((err) => {
          console.log(err);
        })
  }
}

export const registerUser = (data) => {
  return (dispatch) => {
    return fetch(registerUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        data
      })
    })
            .then((response) => response.json())
            .then((data) => {
              console.log('....registerUser.... ', data)
              dispatch(createUser(data))
            })
            .catch((err) => console.log(err))
  }
}

export const deleteArticle = (token, id) => {
  return (dispatch) => {
    return fetch(`${deleteArticleUrl}/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application.json',
        'Content-Type': 'application/json',
        token
      }
    })
        .then((response) => {
          response.json()
        })
    .then((data) => dispatch(removeArticle(id)))
    .catch((err) => console.log(err))
  }
}

export const editArticle = (id, token, data) => {
  return (dispatch) => {
    return fetch(`${editArticleUrl}/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        token
      },
      body: JSON.stringify({
        data
      })
    })
        .then((response) => response.json())
        .then((data) => dispatch(editArticleCreator(data)))
        .catch((err) => console.log(err))
  }
}

export const addArticle = (userdata, token, data) => {
  return (dispatch) => {
    return fetch(addMessageUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'token': token,
        'userdata': JSON.stringify(userdata)
      },
      body: JSON.stringify({
        data
      })
    }).then((response) => response.json())
            .then((data) => dispatch(addArticleCreator(data)))
            .catch((err) => console.log(err))
  }
}

export const openArticleForEdit = (id, article) => {
  return (dispatch) => {
    dispatch(openArticleForEditCreator(id, article))
       // dispatch(translateArticle(id, false));
  }
}

export const openArticleForTranslation = (id, isTranslated) => {
  return (dispatch) => {
    dispatch(translateArticle(id, isTranslated))
  }
}

export const deleteArticleCreator = (token, id) => {
  return (dispatch) => {
    dispatch(deleteArticle(token, id))
      // dispatch(loadData, dispatch);
  }
}
