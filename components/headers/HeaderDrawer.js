import React, {Component} from 'react'
import Head from 'next/head'
import { Col, Row } from 'react-bootstrap'
import DrawerUndocked from '../material-ui-components/DrawerComponent'

import stylesheetLight from 'styles/index.scss'
import stylesheetDark from 'styles/dark.scss'
import Nav from 'components/Nav';

class HeaderDrawer extends Component {
    constructor () {
        super();
        this.state = {
            style : false,
            stylesheet: stylesheetLight
        }
    }

    toggleDrawer =  (e) => {
       this.child.handleToggle();
    };

    changeTheme = () => {
      this.state.style = !this.state.style;

        this.setState({
            stylesheet : this.state.style ? stylesheetDark : stylesheetLight
        });
    };

    render () {
        const { loadData, online } = this.props;
        return (
            <div>
                <style dangerouslySetInnerHTML={{ __html: this.state.stylesheet }} />

                <DrawerUndocked logout={this.props.logout} loadData={loadData} online={online} changeTheme={() => this.changeTheme} ref={(child) => { this.child = child; }} />

                <Nav logout={this.props.logout} online={online} toggleDrawer={this.toggleDrawer} />
                <Head>
                    <title>Sing-Song</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <link rel="manifest" href="/public/manifest.json" />
                    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css' />
                    <link rel="apple-touch-icon" sizes="180x180" href="/public/apple-touch-icon.png" />
                    <link rel="icon" type="image/png" sizes="32x32" href="/public/favicon-32x32.png" />
                    <link rel="icon" type="image/png" sizes="16x16" href="/public/favicon-16x16.png" />
                    <link rel="mask-icon" href="/public/safari-pinned-tab.svg" color="#5bbad5" />
                    <meta name="theme-color" content="#ffffff" />
                </Head>
            </div>
        )
    }
}

export default HeaderDrawer
