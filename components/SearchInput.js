import React, {Component} from 'react';

import {doSearchCreator} from '../actions';

import { connect } from 'react-redux'

class SearchInput extends Component {
    constructor(props) {
        super(props);

        this.doSearch = this.doSearch.bind(this);
    }

    doSearch(e) {
        let queryResult = [];
        let query = e.target.value;
        let articles = this.props.data.length === 0 ? this.props.storedData : this.props.data;

        articles.forEach(function (person) {
            if (person.data.title.toLowerCase().indexOf(query.toLowerCase()) != -1)
                queryResult.push(person);
        });

        const data = {
            query,
            queryResult
        };

        this.props.doSearch(data);
    }

    render() {
        return (
            <div className={'search-input-block'}>
                <input className={'text-input form-control'} type="text" placeholder="Search" ref={'search'} onChange={this.doSearch}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state
};

const mapDispatchToProps = (dispatch) => {
    return {
        doSearch: (data) => {
            dispatch(doSearchCreator(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput)