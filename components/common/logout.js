import { eraseCookieFromAllPaths } from '../../utils/CookieUtils';
import Router from 'next/router';

const Logout = (dispatchLogout) => {
  localStorage.removeItem('user');

  eraseCookieFromAllPaths('x-access-token').then(data => {
    dispatchLogout();
    Router.push({
      pathname: '/login',
    });
  });
};

export default Logout;
