import React, {Component} from 'react'
import { getCookie } from '../../utils/CookieUtils'
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import { setOnlineStatus, loadData, getStoredDataCreator } from '../../actions/';

class DataLoader extends Component {
  constructor(props) {
    super(props);
  }

  static getData = () => {
    let data = this.props.data;
    let result = [];
    const online = this.props.onlineStatus.status;

    if ( this.props.data.length === 0 ) {
      data = this.props.storedData;
    }

    result = this.props.search.queryResult || data || [];
  }

  componentDidMount () {
    let userdata = JSON.parse(localStorage.getItem('user'));
    const online = this.props.onlineStatus.status;
    const posts = this.props.search.queryResult || this.props.data;

    //if(online) {
    if (posts.length === 0) {
      this.loadData();
    }
    //}

    if(online === undefined) {
      this.props.getOnlineStatus(userdata, getCookie('x-access-token'));
    }

    if (!online || online === undefined && this.props.data.length == 0 && this.props.storedData.length == 0 ){
      const data = JSON.parse(localStorage.getItem('LyricserData'));
      this.props.getStoredData(data, userdata, getCookie('x-access-token'));
    }
  }

  loadData = () => {
    let userdata = JSON.parse(localStorage.getItem('user'));

    this.props.loadData(userdata, getCookie('x-access-token'));
  };

  render () {
    let data = this.props.data;
    let result = [];
    const online = this.props.onlineStatus.status;

    if ( this.props.data.length === 0 ) {
      data = this.props.storedData
    }

    result = this.props.search.queryResult || data || [];

    return (
      <div>
        { result ?
          'Data loaded...':
          'Loading...'
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return state
};

const mapDispatchToProps = dispatch => {
  return {
    loadData: bindActionCreators(loadData, dispatch),
    getStoredData: (data, userdata, token) => {
      dispatch(setOnlineStatus(userdata, token));
      dispatch(getStoredDataCreator(data));
    },
    getOnlineStatus : (userdata, token) => {
      dispatch(setOnlineStatus(userdata, token));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DataLoader);