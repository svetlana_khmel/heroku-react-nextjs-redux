import React, { Component } from 'react'
import HeaderDrawer from './headers/HeaderDrawer'
import Category from './Category'
import SearchInput from './SearchInput'
import Articles from 'components/Articles'
import { getCookie } from '../utils/CookieUtils'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setOnlineStatus, loadData, getStoredDataCreator, logOutCreator } from '../actions/'

class Main extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    let userdata = JSON.parse(localStorage.getItem('user'))
    const online = this.props.onlineStatus.status

    this.loadData();

    if (online === undefined) {
      this.props.getOnlineStatus(userdata, getCookie('x-access-token'))
    }

    if (
      !online ||
      (online === undefined &&
        this.props.data.length == 0 &&
        this.props.storedData.length == 0)
    ) {
      const data = JSON.parse(localStorage.getItem('LyricserData'))
      this.props.getStoredData(data, userdata, getCookie('x-access-token'))
    }
  }

  loadData = () => {
    let userdata = JSON.parse(localStorage.getItem('user'))

    this.props.loadData(userdata, getCookie('x-access-token'))
      .then(() => {
        console.log('!!DATA LOADED.');
      })
  };

  render () {
    let data = this.props.data
    let result = []
    const online = this.props.onlineStatus.status

    if (this.props.data.length === 0) {
      data = this.props.storedData
    }

    result = this.props.search.queryResult || data || [];

    console.log('INDEX PROPS.... ', this.props);

    return (
      <div>
        <HeaderDrawer logout={this.props.logOut} loadData={this.loadData} online={online} />
        <SearchInput online={online} />
        <Category data={data} online={online} />
        <Articles loadData={this.loadData} result={result} online={online} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return state
}

const mapDispatchToProps = dispatch => {
  return {
    loadData: bindActionCreators(loadData, dispatch),
    getStoredData: (data, userdata, token) => {
      dispatch(setOnlineStatus(userdata, token))
      dispatch(getStoredDataCreator(data))
    },
    getOnlineStatus: (userdata, token) => {
      dispatch(setOnlineStatus(userdata, token))
    },
    logOut: () => {
      dispatch(logOutCreator());
    }

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
