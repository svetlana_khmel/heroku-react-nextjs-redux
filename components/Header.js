import { Col, Row, Jumbotron } from 'react-bootstrap'

const Header = ({title}) => {
  return (
    <div>
        <h1 style={{textAlign: 'center'}}>{title}</h1>
    </div>
  )
}

export default Header
