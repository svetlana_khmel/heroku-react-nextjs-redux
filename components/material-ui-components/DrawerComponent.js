import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import Link from 'next/link'
import Router from 'next/router';
import Toggle from 'material-ui/Toggle';
import FileCloudDownload from 'material-ui/svg-icons/file/cloud-download';
import {red500, yellow500, blue500} from 'material-ui/styles/colors';

import Logout from '../common/logout';

export default class DrawerComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    handleToggle = () => {
      this.setState({open: !this.state.open})
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleHome = () => {
        this.setState({open: false});
        Router.push({
            pathname: '/'
        })
    };

    handleAbout = () => {
        this.setState({open: false});
        Router.push({
            pathname: '/about'
        })
    };

    handleContact = () => {
        this.setState({open: false});
        Router.push({
            pathname: '/contact'
        })
    };

    handleNewpost = () => {
        this.setState({open: false});
        Router.push({
            pathname: '/new'
        })
    };

    handleSync = () => {
        localStorage.removeItem('LyricserData');
        this.props.loadData();
    };

    logOut = () => {
        Logout(this.props.logout);
    }

    render() {
        const online = this.props.online;

        return (
            <div>
                <Drawer
                    docked={false}
                    width={200}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}
                >
                    {/*<MenuItem onClick={this.handleClose}>Menu Item</MenuItem>*/}
                    {/*<MenuItem onClick={this.handleClose}>Menu Item 2</MenuItem>*/}

                    <div className={'theme-toggler'}>
                        <Toggle
                            label="Toggle theme"
                            defaultToggled={true}
                            onToggle={ this.props.changeTheme()}
                        />
                    </div>

                    <MenuItem onClick={this.handleHome}>Home</MenuItem>
                    <MenuItem onClick={this.handleAbout}>About</MenuItem>
                    <MenuItem onClick={this.handleContact}>Contact</MenuItem>

                    {online &&
                        <MenuItem onClick={this.handleNewpost}>New post</MenuItem>
                    }

                    <div className={'drawer-item'} onClick={() => this.logOut()}><a>Logout</a></div>

                    <div onClick={this.handleSync}>
                        <span className={'FileCloudDownload-ico'}><FileCloudDownload color={red500}/></span><span className={'FileCloudDownload-label'}>Sync data</span>
                    </div>
                </Drawer>
            </div>
        );
    }
}