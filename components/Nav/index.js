import React, {Component} from 'react';
import Link from 'next/link';
import stylesheet from 'styles/navigation.scss';

import Logout from '../common/logout'

class Nav extends Component {
    logOut = () => {
      Logout(this.props.logout);
    }

    render () {
        return (
            <div>
                <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
                <div className={'header-block'}>
                    <div className={'sangwich'} onClick={this.props.toggleDrawer}>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <div className={'links'}>
                        <Link href='/'><a>Home</a></Link>
                        <Link href='/about' prefetch><a>About</a></Link>
                        <Link href='/contact' prefetch><a>Contact</a></Link>

                        {this.props.online &&
                            <Link href='/new' prefetch><a>New post</a></Link>
                        }

                        <div className={'top-menu-item'} onClick={() => this.logOut()}><a>Logout</a></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Nav
