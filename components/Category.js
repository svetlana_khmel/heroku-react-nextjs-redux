import React, { Component } from 'react';
import {connect} from 'react-redux';
import {doSearchCreator} from '../actions';

import Chip from 'material-ui/Chip';

class Category extends Component {
    constructor (props) {
        super(props);

        this.showCategory = this.showCategory.bind(this);
    }

    showCategory(e) {
        let query = e.target.dataset.category;
        let queryResult = [];
        const articles = this.props.data;

            articles.forEach((item) => {
                if (query === 'all') {
                    queryResult.push(item);
                } else if (item.data.category.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                    queryResult.push(item);
                }
            });

        const data = {
            query,
            queryResult
        };

        this.props.doSearch(data);
    }

    render () {

        let categories = [];

        {typeof this.props.data === 'object' && this.props.data.length != 0 &&

            this.props.data.map((el) => {

                    if (!categories.includes(el.data.category)) {


                        if(/,/.test(el.data.category)) {
                            let arr = el.data.category.split(",");

                            arr.forEach((element) => {
                                if (! categories.includes(element.trim())) {
                                    categories.push(element.trim());
                                }
                            });
                        } else {
                            categories.push(el.data.category.trim());
                        }
                    }
                });
        }

        return (<div className="filter-block">

            <div className="filters">
                 <div data-category='all' className="category-item filter" onClick={this.showCategory}>All</div>

                { categories.length != 0 &&
                    categories.map((el, index)=>{
                        if(el.length !== 0) {
                            return <div key={el + index} data-category={el} className="category-item filter" onClick={this.showCategory}>{el}</div>
                        }
                    })
                }
            </div>
        </div>)
    }
}

const mapStateToProps = (state) => {
    return state
};

const mapDispatchToProps = (dispatch) => {
    return {
        doSearch: (data) => {
            dispatch(doSearchCreator(data));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Category)