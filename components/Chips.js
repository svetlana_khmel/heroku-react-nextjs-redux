import React, { Component } from 'react';
import {connect} from 'react-redux';
import {doSearchCreator, setCategory, setChips} from '../actions';

import Chip from 'material-ui/Chip';

class Chips extends Component {
    constructor (props) {
        super(props);
    }

    markCategory = (e) => {
        let query = e.target.dataset.category;

        this.props.setChips(query);
    };

    render () {
        let categories = [];

        {typeof this.props.data === 'object' && this.props.data.length != 0 &&

            this.props.data.map((el) => {

                if (!categories.includes(el.data.category)) {

                    if(/,/.test(el.data.category)) {
                        let arr = el.data.category.split(",");

                        arr.forEach((element) => {
                            if (! categories.includes(element.trim())) {
                                categories.push(element.trim());
                            }
                        });
                    } else {
                        categories.push(el.data.category.trim());
                    }
                }
            });
        }

        return (<div className="filter-block">

            <div className="filters">

                { categories.length != 0 &&
                    categories.map((el, index)=> {
                        if(el.length !== 0) {
                            return <div key={el + index} data-category={el} className="category-item filter" onClick={this.markCategory}>{el}</div>
                        }
                    })
                }
            </div>

        </div>)
    }
}

const mapStateToProps = (state) => {
    return state
};

const mapDispatchToProps = (dispatch) => {
    return {
        doSearch: (data) => {
            dispatch(doSearchCreator(data))
        },
        setCategory: (data) => {
            dispatch(setCategory(data))
        },
        setChips: (data) => {
            dispatch(setChips(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Chips);