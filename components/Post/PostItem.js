import React, {Component} from 'react'
import EditPostItem from './EditPostItem';
import EditBlock from './EditBlock';
import ReactHtmlParser from 'react-html-parser';

const PostItem = (props) => {
    const {post, online} = props;

    return (
        <div>
            <EditBlock online={online} post={post} />
            <div className={(post.open === true? 'edit-open ':'') + 'article'} key={post._id}>
                <div className={'title'} >{post.data.title}</div>
                <div className={(post.translate === true? 'translate ':'') + 'post'}>{typeof post.data.article === "string" ? post.data.article : ReactHtmlParser(post.data.article.join("<br>"))}</div>
                <div className={(post.translate === true? 'translate ':'') + 'translation'}>{typeof post.data.translation === "string" ? post.data.translation : ReactHtmlParser(post.data.translation.join("<br>"))}</div>

                <div className={'category'}>{post.data.category}</div>
            </div>

            <EditPostItem post={post} />
        </div>
    )};

export default PostItem;

