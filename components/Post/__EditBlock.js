import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  translateArticleOffline,
  openArticleForEdit,
  openArticleForTranslation,
  deleteArticleCreator,
} from '../../actions/index';
import { getCookie } from '../../utils/CookieUtils';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';

class EditBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      translate: false,
      edit: false,
    };
  }

  clear = () => {
    this.setState({
      translate: false,
      edit: false,
    });
  };

  removeFullArticle = () => {
    const token = getCookie('x-access-token');
    const id = this.props.post._id;

    this.props.deleteArticle(token, id);
  };

  translateArticle = () => {
    const id = this.props.post._id;
    const translate = !this.state.translate;

    this.setState({
      translate,
    });

    if (this.props.onlineStatus.status) {
      this.props.translateArticle(id, translate);
    } else {
      this.props.translateArticleOffline(id, translate, this.props.storedData);
    }
  };

  openArticle = () => {
    const id = this.props.post._id;
    const edit = !this.state.edit;

    this.setState({
      edit,
    });

    this.props.openArticleForEdit(id, edit);
  };

  render() {
    var divClassTranslate = classNames('btn translate', {
      hidden:
      this.state.edit === true,
    });

    var divClassEdit = classNames('btn edit', {
      hidden:
      this.state.translate === true,
    });

    return (
      <div className="edit-block">
        {online &&
        (
          <div className={divClassEdit} onClick={this.openArticle}>
            {this.state.edit === true ? 'Back' : 'Edit'}
          </div>
        )}
        {online &&
        online != undefined && (
          <div className="btn remove" onClick={this.removeFullArticle}>
            Remove
          </div>
        )}
        <div className={divClassTranslate} onClick={this.translateArticle}>
          {this.state.translate === true ? 'Back' : 'Translate'}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(mapStateToProps, (dispatch) => bindActionCreators({openArticleForEdit}, dispatch))(EditBlock);

