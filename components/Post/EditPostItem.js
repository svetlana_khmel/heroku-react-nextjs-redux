import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editArticle, openArticleForEdit } from '../../actions/index';
import { getCookie } from '../../utils/CookieUtils';
import { bindActionCreator } from 'redux';

class EditPostItem extends Component {
  constructor(props) {
    super(props);

    const { title, article, translation, category } = props.post.data;

    this.state = {
      title,
      article,
      translation,
      category,
    };
  }

  submitData = () => {
    const token = getCookie('x-access-token');
    const id = this.props.post._id;

    const data = {
      title: this.refs.title.value,
      article: /\n/.test(this.refs.article.value)
        ? this.refs.article.value.split(/\n/)
        : this.refs.article.value,
      translation: /\n/.test(this.refs.translation.value)
        ? this.refs.translation.value.split(/\n/)
        : this.refs.translation.value,
      category: this.refs.category.value,
    };

    this.props.editArticle(id, token, data, false).then(this.props.loadData);
  };

  changeValue = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  componentWillReceiveProps(nextProps) {
    const { title, article, translation, category } = nextProps.post.data;

    this.setState({
      title,
      article,
      translation,
      category,
    });
  }

  render() {
    const post = this.props.post;

    return (
      <div
        className={(post.open ? 'open ' : '') + 'article edit-article'}
        key={post._id}
        data-id={post._id}
      >
        <input
          className="form-control"
          type="text"
          ref={el => this.title = el}
          value={this.state.title}
          onChange={() => this.changeValue('title', e)}
        />
        <textarea
          className="form-control"
          ref={'article'}
          value={
            typeof this.state.article === 'string'
              ? this.state.article
              : this.state.article.join('\n')
          }
          onChange={e => this.changeValue('article', e.target.value)}
        />
        <textarea
          className="form-control"
          ref={'translation'}
          value={
            typeof this.state.translation === 'string'
              ? this.state.translation
              : this.state.translation.join('\n')
          }
          onChange={e => this.changeValue('translation', e.target.value)}
        />
        <input
          className="form-control"
          ref="category"
          type="text"
          value={this.state.category}
          onChange={e => this.changeValue('category', e.target.value)}
        />

        <button
          id="something-btn"
          type="button"
          className="btn btn-success btn-sm"
          onClick={this.submitData}
        >
          Send
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

// const mapDispatchToProps = dispatch => {
//   return {
//     editArticle: (token, id, article, bull) => {
//       dispatch(editArticle(token, id, article));
//      // dispatch(openArticleForEdit(id, bull));
//     },
//   };
// };

export default connect(mapStateToProps, (dispatch)=>bindActionCreator({ openArticleForEdit }, dispatch))(EditPostItem);
