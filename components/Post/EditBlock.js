import React, { Component } from 'react';
import classNames from 'classnames';

export default class EditBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      translate: false,
      edit: false,
    };
  }

  handleClick = () => {

    const translate = !this.state.translate;

    this.setState({
      translate,
    });

    //if (this.props.onlineStatus.status) {
    //  this.props.translateArticle(id, translate);
    // } else {
    //   this.props.translateArticleOffline(id, translate, this.props.storedData);
    // }

    this.props.translateArticle(this.props.id)
  }

  render() {
    var divClassTranslate = classNames('btn translate', {
      hidden:
      this.state.edit === true,
    });

    return (
      <div className="edit-block">
       <div className={divClassTranslate} onClick={()=> this.handleClick()}>

         {this.state.translate === true ? 'Back' : 'Translate'}

        </div>
      </div>
    );
  }
}

