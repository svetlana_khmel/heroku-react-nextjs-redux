import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editArticle, deleteArticleCreator, loadData } from '../actions';
import { getCookie } from '../utils/CookieUtils';
import Router from 'next/router';
import Chips from './Chips';
import RaisedButton from 'material-ui/RaisedButton';
import Header from './Header';

class Edit extends Component {
  constructor(props) {
    super(props);

    const {
      title,
      article,
      translation,
      category,
    } = props.openArticleToEdit.data;

    this.state = {
      title,
      article,
      translation,
      category,
      categories: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    const {
      title,
      article,
      translation,
      category,
    } = nextProps.openArticleToEdit.data;


    this.setState({
      title,
      article,
      translation,
      category,
    });
  }

  componentDidMount() {
    if (this.state.category) {
      this.category.value = this.state.category;
    }
    this.setState({
      categories: [...this.state.category.trim().split(',')],
    });
  }

  gotoBack = () => {
    Router.push({
      pathname: '/',
    });
  };

  loadData = () => {
    let userdata = JSON.parse(localStorage.getItem('user'));

    this.props.loadData(userdata, getCookie('x-access-token'));
  };

  deleteArticle = () => {
    const token = getCookie('x-access-token');
    const id = this.props.openArticleToEdit._id;
    this.props.deleteArticleCreator(token, id);
    this.loadData();
    this.gotoBack();
  };

  submitData = () => {
    const token = getCookie('x-access-token');
    const id = this.props.openArticleToEdit._id;

    const data = {
      title: this.title.value ? this.title.value : '',
      article: /\n/.test(this.article.value)
        ? this.article.value.split(/\n/)
        : this.article.value,
      translation: /\n/.test(this.translation.value)
        ? this.translation.value.split(/\n/)
        : this.translation.value,
      category: this.category.value,
    };

    this.props.editArticle(id, token, data).then(() => {
      this.gotoBack();
    });
  };

  changeValue = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  fillCategories = () => {
    let categoryValues = this.category.value;

    this.setState({
      categories: [...categoryValues.trim().split(',')],
    });
  };

  choosenChips = () => {
    let length = [...this.props.chips, ...this.state.categories].length;

    let chips = [...this.props.chips, ...this.state.categories].map(
      (el, index) => {
        return length === index + 1 ? <span>{el}</span> : <span>{el}, </span>;
      },
    );

    return chips;
  };

  render() {
    return (
      <div className={'new-post-block'}>
        <div className={'back-link'} onClick={() => this.gotoBack()}>
          Back
        </div>
        <div className={'delete-link'} onClick={() => this.deleteArticle()}>
          Detete
        </div>
        <Header title={'Edit Post'} />

        <div className={'new-post-form'}>
          <label>Title</label>
          <input
            className="form-control"
            type="text"
            ref={el => (this.title = el)}
            value={this.state.title}
            onChange={e => this.changeValue('title', e.target.value)}
          />

          <label>Lyrics</label>
          <textarea
            className="form-control"
            ref={el => (this.article = el)}
            value={
              typeof this.state.article === 'string'
                ? this.state.article
                : this.state.article.join('\n')
            }
            onChange={e => this.changeValue('article', e.target.value)}
          />
          <label>Translation</label>
          <textarea
            className="form-control"
            ref={el => (this.translation = el)}
            value={
              typeof this.state.translation === 'string'
                ? this.state.translation
                : this.state.translation.join('\n')
            }
            onChange={e => this.changeValue('translation', e.target.value)}
          />
          <label>Category</label>
          <Chips />
          <div className={'chips '}>{this.choosenChips()}</div>
          <input
            className={'form-control'}
            type={'text'}
            ref={el => (this.category = el)}
            name="category"
            onChange={() => this.fillCategories()}
          />
          <div className={'floating-bottom-button-block'}>
            <RaisedButton
              className={'floating-bottom-button'}
              label="Send"
              primary={true}
              onClick={this.submitData}
            />
          </div>
          {/*<button id="something-btn" type="button" class="btn btn-success btn-sm" onClick={this.submitData}>
                    Send
                </button>*/}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};


export default connect(mapStateToProps, dispatch =>
  bindActionCreators({ editArticle, deleteArticleCreator, loadData }, dispatch),
)(Edit);
