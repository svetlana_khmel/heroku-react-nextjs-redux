import React, { Component } from 'react';
import { getPosts } from 'api/data';
import { connect } from 'react-redux';
import Router from 'next/router';

import { openArticleForTranslation, openArticleForEdit } from '../actions/';
import EditBlock from './Post/EditBlock';
import ReactHtmlParser from 'react-html-parser';
import { bindActionCreators } from 'redux';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

class Articles extends Component {
  state = {
    translate: false,
  };

  addNewPost = () => {
    Router.push({
      pathname: '/new',
    });
  };

  gotoEdit = (id, post) => {
    Router.push({
      pathname: '/edit',
    });

    this.props.openArticleForEdit(
      this.props.openArticleForEdit === id ? '' : id,
      post,
    );
  };

  translateArticle = id => {
    this.props.openArticleForTranslation(
      this.props.translatedArticle === id ? '' : id,
      true,
    );
  };

  renderList = posts => {
    const online = this.props.online;

    return posts.map((post, index) => (
      <div>
        <div
          className={(post.open === true ? 'edit-open ' : '') + 'article'}
          key={post._id}
        >
          <div
            className={'edit-link'}
            onClick={() => this.gotoEdit(post._id, post)}
          >
            Edit
          </div>
          <EditBlock
            id={post._id}
            online={online}
            translateArticle={this.translateArticle}
            post={post}
          />

          <div className={'title'}>{post.data.title}</div>
          <div
            className={
              (post._id === this.props.translatedArticle ? 'hidden ' : ' ') +
              'post'
            }
          >
            {typeof post.data.article === 'string'
              ? post.data.article
              : ReactHtmlParser(post.data.article.join('<br>'))}
          </div>

          <div
            id={post._id}
            ref={translateBlock => {
              this.translateBlock = translateBlock;
            }}
            className={
              (post._id === this.props.translatedArticle
                ? 'translate '
                : 'hidden ') + 'translation'
            }
          >
            {typeof post.data.translation === 'string'
              ? post.data.translation
              : ReactHtmlParser(post.data.translation.join('<br>'))}
          </div>

          <div className={'category'}>{post.data.category}</div>
        </div>
      </div>
    ));
  };

  render() {
    const { online, result } = this.props;

    return (
      <div className={'articles-block'}>
        {result ? (
          <ul>
            {this.renderList(result)}
            {/* {posts.map(p => <PostItem props={this.props} key={p._id} post={p} />)}*/}
          </ul>
        ) : (
          'Loading...'
        )}
        {online === true && (
          <FloatingActionButton
            className={'floating-button'}
            onClick={() => this.addNewPost()}
          >
            <ContentAdd />
          </FloatingActionButton>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(mapStateToProps, dispatch =>
  bindActionCreators(
    { openArticleForTranslation, openArticleForEdit },
    dispatch,
  ),
)(Articles);
