import React, { Component } from 'react';
import { loginUser } from '../actions';
import { connect } from 'react-redux';

import Router from 'next/router';

import md5 from 'md5';

import { setCookie } from '../utils/CookieUtils';
import { validate } from '../validation';
import RaisedButton from 'material-ui/RaisedButton';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      errors: '',
      error: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    console.log('login props: ', nextProps);
    const { user } = nextProps;

    if (user.error) {
      console.log('Login error');
      this.setState({
        error: user.error,
      });
    }

    if (user.success === true) {
      const currentUser = {
        username: user.user.username,
        id: user.user._id,
      };

      localStorage.setItem('user', JSON.stringify(currentUser));
    }

    if (user.success) {
      setCookie('x-access-token', user.token);
      Router.push({
        pathname: '/',
      });
    }
  }

  componentDidMount() {
    const { user } = this.props;

    if (user.success && this.props.url) {
      this.props.url.replaceTo('/'); // redirect if you're already logged in
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    // const username = this.refs.email.value;
    // const password = this.refs.password.value;
    //
    // const data = {
    //     username,
    //     password: md5(password)
    // };

    if (Object.keys(validate(this.state)).length !== 0) return;

    const data = {
      username: this.state.username,
      password: md5(this.state.password),
    };

    console.log('Login data, ', data);

    this.props.loginUser(data);
  }

  handleForm(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });

    this.validateForm();
  }

  validateForm() {
    validate(this.state);

    this.setState({
      errors: validate(this.state),
    });
  }

  render() {
    return (
      <div className="auth-block register-block">
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit}>
          <input
            className={'form-control'}
            type="text"
            name={'username'}
            placeholder={'Login'}
            ref="email"
            onKeyUp={e => this.handleForm(e)}
            onChange={e => this.handleForm(e)}
          />
          <div className={'warn'}>{this.state.errors.username}</div>
          <input
            className={'form-control'}
            type="password"
            name={'password'}
            placeholder={'Password'}
            ref="password"
            onKeyUp={e => this.handleForm(e)}
            onChange={e => this.handleForm(e)}
          />
          <div className={'warn'}>{this.state.errors.password}</div>
          <input
            className={'btn'}
            type="submit"
            value="Submit"
            onKeyUp={e => this.handleForm(e)}
            onChange={e => this.handleForm(e)}
          />
          {/* <RaisedButton label="Primary" primary={true} />*/}
          {/* <RaisedButton label="Secondary" secondary={true} />*/}
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('Login state: ', state);
  return state;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginUser: data => {
      dispatch(loginUser(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
