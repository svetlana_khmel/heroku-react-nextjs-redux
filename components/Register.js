import React, {Component} from 'react';
import {registerUser} from '../actions';
import {connect} from 'react-redux';

import md5 from 'md5';

import {validate} from '../validation';
import Router from 'next/router';
import { setCookie } from '../utils/CookieUtils';

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
            errors:'',
            error: ''
        }
    }

    componentWillReceiveProps (nextProps) {
        const {user} = nextProps;

        if(user.error) {
            this.setState({
                error: user.error
            })
        }

        if (user.success) {
            setCookie('x-access-token', user.token);
            Router.push({
                pathname: '/'
            })
        }
    }

    handleSubmit (e) {
        e.preventDefault();

        if(Object.keys(validate(this.state)).length !== 0) return;

        const data = {
            username: this.state.username,
            password: md5(this.state.password)
        };

        this.props.registerUser(data);
    }

    handleForm (e) {
        this.setState({
            [e.target.name]: e.target.value
        });

        this.validateForm();
    }

    validateForm () {
        validate(this.state);

        this.setState({
            errors: validate(this.state)
        })
    }

    render () {
        return (
            <div className="auth-block register-block">
                <h2>Register</h2>
                <div className="auth-error">{this.state.error}</div>
                <form onSubmit={this.handleSubmit} >
                    <input className={'form-control'} type="text" name={'username'} placeholder={'Login'} onKeyUp={(e) => this.handleForm(e)} onChange={(e) => this.handleForm(e)} ref="username"/>
                    <div className={'warn'}>{this.state.errors.username}</div>
                    <input className={'form-control'} type="password" name={'password'} placeholder={'Password'} onKeyUp={(e) => this.handleForm(e)} onChange={(e) => this.handleForm(e)} ref="password"/>
                    <div className={'warn'}>{this.state.errors.password}</div>
                    <input className={'form-control'} type="password" name={'confirmPassword'} placeholder={'Confirm pasword'} onKeyUp={(e) => this.handleForm(e)} onChange={(e) => this.handleForm(e)} ref="confirmPassword"/>
                    <div className={'warn'}>{this.state.errors.confirmPassword}</div>
                    <input className={'btn'} type="submit" value="Submit"/>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log('Register state: ', state);
    return state
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        registerUser: (data) => {
            dispatch(registerUser(data));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);