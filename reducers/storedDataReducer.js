import * as types from '../actionTypes';

let initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case types.GET_STOERD_ARTICLES:
            return [...state, ...action.payload];

        default:
            return state
    }
}