import * as types from '../actionTypes';

let initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.DO_SEADRCH:
            return {...state, ...action.payload};

        default:
            return state
    }
}