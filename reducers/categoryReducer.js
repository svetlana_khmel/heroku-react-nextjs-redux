import * as types from '../actionTypes';

let initialState = [];

export default (state = initialState, action) => {

    switch (action.type) {
        case types.SET_CATEGORY:

            var index = state.indexOf(action.payload);

            if(index != -1) {
               return [ ...state.slice(0,index), ...state.slice(index+1 ) ]
            } else {
                return [...state, action.payload];
            }

        default:
            return state
    }
}



