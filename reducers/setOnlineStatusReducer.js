import * as types from '../actionTypes';

let initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_ONLINE_STATUS:
            console.log("________SET_ONLINE_STATUS....................", {...state, status: action.isOnline})
            return {...state, status: action.isOnline};

        default:
            return state
    }
}