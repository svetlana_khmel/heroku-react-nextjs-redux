import { OPEN_ARTICLE_TO_EDIT } from '../actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case OPEN_ARTICLE_TO_EDIT:
      //return (action.id).substr(0);
      console.log('....OPEN_ARTICLE_TO_EDIT..... ', { ...action.article });
      return { ...action.article }

    default:
      return state
  }
}

