import { GET_ARTICLES, TOGGLE_OPEN_ARTICLE, OPEN_ARTICLE_TO_EDIT }  from '../actionTypes';

let initialState = [];

function articleReducer(state = {}, action) {
  console.log('DATA REDUCER   ------0');
  switch (action.type) {
    case 'TOGGLE_EDIT_ARTICLE':
    //   return Object.assign({}, state, {
    //     isEdit: !state.edit,
    //   });

    case TOGGLE_OPEN_ARTICLE:
      return Object.assign({}, state, {
        isOpen: !state.open,
      });
  }
}

export default (state = initialState, action) => {

  switch (action.type) {
    case GET_ARTICLES:
      let unic = action.payload.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj['_id']).indexOf(obj['_id']) === pos;
      });

      localStorage.setItem('LyricserData', JSON.stringify(unic)); // NO NO NO NO N N O NO NOOOOOO

      return unic;

    case TOGGLE_OPEN_ARTICLE:
      // return state.map(article => {
      //   if (article._id === action.id) {
      //     return articleReducer(article, { type: 'TOGGLE_OPEN_ARTICLE' });
      //   }
      //
      //   return article;
     // });

    // case types.ADD_ARTICLE:
    //   return [...state, action.payload];
    //
    // case types.REMOVE_ARTICLE:
    //   return state.filter(article => {
    //     if (article._id !== action.id) {
    //       return article;
    //     }
    //   });
    //
    // case types.EDIT_ARTICLE:
    //   console.log('EDIT_ARTICLE... ', action);
    //   console.log('EDIT_ARTICLE..state. ', state);
    //
    //   // return []
    //   return state.map((article, index) => {
    //     if (article._id === action.data._id) {
    //       return articleReducer(article, { type: 'TOGGLE_EDIT_ARTICLE' });
    //     }
    //
    //     return article;
    //   });

    default:
      return state;
  }
};
