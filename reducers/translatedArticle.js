import { TRANSLATE_ARTICLE } from '../actionTypes';

export default (state = '', action) => {
  switch (action.type) {
    case TRANSLATE_ARTICLE:
      return (action.id).substr(0);

    default:
      return state
  }
}

