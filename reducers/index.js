import { combineReducers } from 'redux';
import form from './formReducer';
import data from './dataReducer';
import user from './loginUserReducer';
import search from './searchReducer';
import category from './categoryReducer';
import storedData from './storedDataReducer';
import onlineStatus from './setOnlineStatusReducer';
import chips from './chipsReducer';
import clearArticle from './clearArticle';
import translatedArticle from './translatedArticle';
import openArticleToEdit from './openArticleToEdit';

export default combineReducers({
  form,
  data,
  user,
  search,
  category,
  storedData, // no reducer suffix is required
  onlineStatus,
  chips,
  clearArticle,
  translatedArticle,
  openArticleToEdit
});
