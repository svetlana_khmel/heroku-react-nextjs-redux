import {SET_CHIPS} from '../actionTypes';

let initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_CHIPS:
            let data  = action.payload;

            if (state.includes(data)) {
                let arr = state.filter(item => item !== data);

                return [...arr];
            } else {
                return [...state, action.payload];
            }

        default:
            return state;
    }
}